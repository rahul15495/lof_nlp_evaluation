#many to one mapping .
entity_type_lookup= {
    ('TEST_TREATMENT_PROCEDURE', 'test'): 'test',
    ('TEST_TREATMENT_PROCEDURE', 'treatment'): 'treatment',
    ('TEST_TREATMENT_PROCEDURE', 'labvalue'): 'labvalue',
    ('MEDICAL_CONDITION','problem'): 'problem',
    ('Finding'): 'finding',
    ('PROTECTED_HEALTH_INFORMATION'): 'PROTECTED_HEALTH_INFORMATION',
    ('MEDICATION','drug'): 'drug',
    ('ANATOMY','BDL'): 'BDL',
    ('temporal'): 'temporal',
    ('DOSAGE','drug::STRENGTH'): 'drug::STRENGTH',
    ('MEDICATION','drug::RUT'): 'drug::RUT',
    ('MEDICATION','drug::FREQ'): 'drug::FREQ',
    ('MEDICATION','SEV'): 'SEV',
    ('NEG'): 'NEG',
    ('UNC'): 'UNC',
}

get_dereferenced_value=lambda x:[z for y,z in entity_type_lookup.items() if x in y][0]