import sys
from parsers import gold, aws
from vendor import aws_comprehend
import evaluator


if __name__=='__main__':
    annotation_note_path= sys.argv[1]
    annotation_gold_path= sys.argv[2]

    gold_annotation= gold.parse(annotation_gold_path)

    notes = open(annotation_note_path).readlines()
    notes = ''.join(notes)
    notes_annotation = aws.parse_annotation(aws_comprehend.call_ComprehendMedical(notes))
    print(evaluator.evaluate_document(gold_annotation, notes_annotation))
