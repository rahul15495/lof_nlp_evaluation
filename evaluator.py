from entity_lookup import get_dereferenced_value

def weak_annotation_match(p1,l1, p2, l2):
	
	e1,e2= l1-1, l2-1
	'''
	Let e1 = p1+l1−1 and e2 = p2+l2−1 be the last character index of the two mentions.
	Two mentions overlap iff p1 ≤ p2 ≤ e1 ∨ p1 ≤ e2 ≤ e1 ∨ p2 ≤ p1 ≤ e2 ∨ p2 ≤ e1 ≤ e2. 
	(here V corresponds to a Logical OR)

	'''    
	return  (p1 <= p2 <= e1) or  (p1 <= e2 <= e1) or (p2 <= p1 <= e2) or  (p2 <= e1 <= e2)


from functools import partial

def has_a_match(reference_entity, annotated_entity):
	
	has_wam= weak_annotation_match(reference_entity['start'] ,reference_entity['end'] , 
									annotated_entity['start'] ,annotated_entity['end'])
	
	has_entity_match= get_dereferenced_value(reference_entity['entity_type'])== get_dereferenced_value(annotated_entity['entity_type'])
	
	return int(has_wam and has_entity_match)


def evaluate_document(reference, annotation):
	
	truePositives = 0
	falsePositives= 0
	falseNegatives= 0
	
	for reference_entity in reference:
		
		func= partial(has_a_match, reference_entity)
		
		if sum(map(func, annotation))>0:
			truePositives +=1
		else:
			falseNegatives +=1
			
	falsePositives= len(annotation)-truePositives
	
	
	precision = truePositives/(truePositives + falseNegatives)
	recall    = truePositives/(truePositives + falsePositives)
	f1_score  = 2*((precision*recall)/(precision+recall))
	
			
	return {
			'truePositives' : truePositives,
			'falsePositives': falsePositives,
			'falseNegatives': falseNegatives,
			'precision'     : precision,
			'recall'        : recall,
			'f1-score'      : f1_score
			}