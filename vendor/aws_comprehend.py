import os
import boto3
import traceback

'''
######################### api documentation reference: ##########################################
https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/comprehendmedical.html
https://docs.aws.amazon.com/goto/WebAPI/comprehendmedical-2018-10-30/DetectEntitiesV2

#################################################################################################
'''


'''
########################################## possible errors : ####################################

InternalServerException
HTTP Status Code: 500

InvalidEncodingException
The input text was not in valid UTF-8 character encoding. Check your text then retry your request.

HTTP Status Code: 400

InvalidRequestException
The request that you made is invalid. Check your request to determine why it's invalid and
then retry the request.

HTTP Status Code: 400

ServiceUnavailableException
The Amazon Comprehend Medical service is temporarily unavailable. Please wait and
then retry your request.

HTTP Status Code: 400

TextSizeLimitExceededException
The size of the text you submitted exceeds the size limit. Reduce the size of the
text or use a smaller document and then retry your request.

HTTP Status Code: 400

TooManyRequestsException
You have made too many requests within a short period of time. Wait for a short time and
then try your request again. 
Contact customer support for more information about a service limit increase.

HTTP Status Code: 400
##########################################################################################

'''

AWS_ACCESS_KEY_ID = os.environ['AWS_ACCESS_KEY_ID']
AWS_SECRET_ACCESS_KEY= os.environ['AWS_SECRET_ACCESS_KEY']
REGION_NAME = os.environ['REGION_NAME']

def get_client(resource):
    session = boto3.Session(
                    aws_access_key_id= AWS_ACCESS_KEY_ID,
                    aws_secret_access_key= AWS_SECRET_ACCESS_KEY,
                    region_name= REGION_NAME
                )
    client = session.client(resource)

    return client

def call_ComprehendMedical(input_text):

    try:
        client= get_client('comprehendmedical')

        response= client.detect_entities_v2(Text=input_text)

        return response
    except:
        # < proper exception handling channel to be entered here :: eg: email alert>

        print(traceback.format_exc())

        return None