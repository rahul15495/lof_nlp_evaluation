import yaml

CONFIDENCE_THRESHOLD=0.0

def load(path):
    #strip of first four lines containingthe input text as an independent dictionary
    annotation_aws= open(path).readlines()
    
    annotation_aws= list(map(lambda x: x.strip('\n'), annotation_aws)) #new line character strip
    
    annotation_aws= ''.join(annotation_aws)
    
    return yaml.load(annotation_aws) #return annotation in dictionary format

def get_entity_attributes(entity):
    attributes= {}

    if entity['Score']> CONFIDENCE_THRESHOLD:
        attributes['start']= entity['BeginOffset']
        attributes['end']= entity['EndOffset']
        attributes['entity_type']=entity['Category']
        attributes['entity']=entity['Text']
        return attributes
    else:
        return {}

def parse(annotation_aws_path):
    annotation_aws= load(annotation_aws_path)
    
    #filter all the entites for which the predicition score is less than the threshold
    entities = list(filter(None,map(get_entity_attributes,annotation_aws['Entities'])))
    
    return entities


def parse_annotation(aws_annotation):
    # filter all the entites for which the predicition score is less than the threshold
    entities = list(filter(None, map(get_entity_attributes, aws_annotation['Entities'])))

    return entities