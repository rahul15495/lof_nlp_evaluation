import json

#return annotation in dictionary format
load= lambda path : json.load(open(path))

def get_entity_attributes(entity):
    attributes= {}

    attributes['start']= int(entity['Location_Start'])
    attributes['end']= int(entity['Location_End'])
    attributes['entity_type']=entity['Semantics']
    attributes['entity']=entity['Entity']
    return attributes


def parse(annotation_gold_path):
    annotation_gold= load(annotation_gold_path)
    
    #filter all the entites for which the predicition score is less than the threshold
    entities = list(filter(None,map(get_entity_attributes,annotation_gold)))
    
    return entities